//
//  ViewControllerJason2.swift
//  ExamenIOS
//
//  Created by Christian on 5/6/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import UIKit

class ViewControllerJason2: UIViewController, UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayuofItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "mycell", for: indexPath) as! TableViewCell
        cell.label.text = arrayuofItems[indexPath.row]
        cell.value.text = "\(numbers[indexPath.row])"
        return cell
        
    }
    
    
     var nombre: String = ""
    var arrayuofItems: [String] = ["","","",""]
    var numbers : [Int] = [0,0,0,0]
    
    
    
    
    
  
    @IBOutlet weak var celdaTableView: UITableView!
    
    @IBOutlet weak var labelName: UILabel!
      //let nombre: string
    
    
    
    @IBOutlet weak var labelPromedio: UILabel!
    
    /*
    @IBAction func okbutton(_ sender: Any) {
        
        
        
        
        let urlString2 = "https://api.myjson.com/bins/182sje"
        let url2 = URL (string: urlString2)
        let session2 = URLSession.shared
        let task2 = session2.dataTask(with: url2!) { (data, response, error) in
            
            guard let data2 = data else{
                
                print("error no data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(LabelsApi.self, from: data2)
                else{
                    print("Error decoding Weather")
                    return
            }
            
            print(weatherInfo.data[0].value)
            
            DispatchQueue.main.async{
                self.label1.text = "\(weatherInfo.data[0].value)"
                self.label2.text = "\(weatherInfo.data[1].value)"
                self.label3.text = "\(weatherInfo.data[2].value)"
                self.label4.text = "\(weatherInfo.data[3].value)"
                
                
                let uno: Int = weatherInfo.data[0].value
                let dos: Int = weatherInfo.data[1].value
                let tres: Int = weatherInfo.data[2].value
                let cuatro: Int = weatherInfo.data[3].value
                
                let suma : Int = (uno + dos + tres + cuatro)
                let cte : Int = 4
                let promedio: Double = Double(suma/cte)
                
                self.labelPromedio.text = "\(promedio)"
                
                self.arrayuofItems[0] = "\(weatherInfo.data[0].label)"
                self.arrayuofItems[1] = "\(weatherInfo.data[1].label)"
                self.arrayuofItems[2] = "\(weatherInfo.data[2].label)"
                self.arrayuofItems[3] = "\(weatherInfo.data[3].label)"
                self.numbers[0] = weatherInfo.data[0].value
                self.numbers[1] = weatherInfo.data[1].value
                self.numbers[2] = weatherInfo.data[2].value
                self.numbers[3] = weatherInfo.data[3].value
                
                self.celdaTableView.reloadData()
                
            }
        }
        task2.resume()
        
        
        
        
    }
    */
    
    
   

    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelName?.text = "Hi  " + nombre
        
        
        
        let urlString2 = "https://api.myjson.com/bins/182sje"
        let url2 = URL (string: urlString2)
        let session2 = URLSession.shared
        let task2 = session2.dataTask(with: url2!) { (data, response, error) in
            
            guard let data2 = data else{
                
                print("error no data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(LabelsApi.self, from: data2)
                else{
                    print("Error decoding Weather")
                    return
            }
            
            print(weatherInfo.data[0].value)
            
            DispatchQueue.main.async{
               /* self.label1.text = "\(weatherInfo.data[0].value)"
                self.label2.text = "\(weatherInfo.data[1].value)"
                self.label3.text = "\(weatherInfo.data[2].value)"
                self.label4.text = "\(weatherInfo.data[3].value)"*/
                
                
                let uno: Int = weatherInfo.data[0].value
                let dos: Int = weatherInfo.data[1].value
                let tres: Int = weatherInfo.data[2].value
                let cuatro: Int = weatherInfo.data[3].value
                
                let suma : Int = (uno + dos + tres + cuatro)
                let cte : Int = 4
                let promedio: Double = Double(suma/cte)
                
                self.labelPromedio.text = "\(promedio)"
                
                self.arrayuofItems[0] = "\(weatherInfo.data[0].label)"
                self.arrayuofItems[1] = "\(weatherInfo.data[1].label)"
                self.arrayuofItems[2] = "\(weatherInfo.data[2].label)"
                self.arrayuofItems[3] = "\(weatherInfo.data[3].label)"
                self.numbers[0] = weatherInfo.data[0].value
                self.numbers[1] = weatherInfo.data[1].value
                self.numbers[2] = weatherInfo.data[2].value
                self.numbers[3] = weatherInfo.data[3].value
                
                self.celdaTableView.reloadData()
                
            }
        }
        task2.resume()
        
        
        
        
        //labelPromedio?.text = promedio.text

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
