//
//  ViewController.swift
//  ExamenIOS
//
//  Created by Christian on 5/6/18.
//  Copyright © 2018 Christian. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    
    
    
    
    @IBOutlet weak var label1: UILabel!
    
    @IBOutlet weak var label2: UILabel!
    
    @IBOutlet weak var label3: UILabel!
    

    @IBOutlet weak var nameTextField: UITextField!
    
    
    
   /* @IBAction func OKbutton(_ sender: Any) {
        
        
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL (string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            guard let data = data else{
                
                print("error no data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(ChristianWeatherInfo.self, from: data)
                else{
                    print("Error decoding Weather")
                    return
            }
            
            print(weatherInfo)
            
            DispatchQueue.main.async{
                self.label2.text = "\(weatherInfo.date)"
                self.label1.text = "\(weatherInfo.viewTitle)"
                
            }
        }
        task.resume()
        
        
        
    }]*/
    
    
      
    @IBAction func nextScreen(_ sender: Any) {
        
       performSegue(withIdentifier: "pasarNombre", sender: self)
        
        
        
    }
    
    func showAlert(title: String, message: String){
        let alert = UIAlertController(title:title,message:message,preferredStyle: .alert)
        
        let  okAction = UIAlertAction(title:"OK", style: .default, handler: nil)
        alert.addAction(okAction)
        
        present(alert, animated:true,completion: nil)
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (nameTextField.text == ""){
            //print("Empty fields, insert data !!  ")
            
            showAlert(title: "ERROR", message: "Name is required")
            navigationController?.popViewController(animated: true)
        }else{
            if segue.identifier == "pasarNombre" {
                let destino = segue.destination as? ViewControllerJason2
                destino?.nombre = nameTextField.text!
        }
        
        
        }

        
        
        
        
        /*
         let itemTitle = titleTextField.text ?? ""
         let itemLocation = locationTextField.text ?? ""
         let itemDescription = descriptionTextFiel.text ?? ""
         
         if (titleTextField.text == "" || descriptionTextFiel.text == " "){
         //print("Empty fields, insert data !!  ")
         showAlert(title: "ERROR", message: "Title and Description is required")
         navigationController?.popViewController(animated: true)
         }else{
         
         //print("Data saved")
         //showAlert(title: "Succed", message: "Data Saved")
         
         let item = Item(title: itemTitle, location: itemLocation, description: itemDescription)
         
         itemManager.toDoItems += [item]
         navigationController?.popViewController(animated: true)
         
         }
         */
        
        
    }
    

    
    

    override func viewDidLoad() {
        
        let urlString = "https://api.myjson.com/bins/72936"
        let url = URL (string: urlString)
        let session = URLSession.shared
        let task = session.dataTask(with: url!) { (data, response, error) in
            
            guard let data = data else{
                
                print("error no data")
                return
            }
            
            guard let weatherInfo = try? JSONDecoder().decode(ChristianWeatherInfo.self, from: data)
                else{
                    print("Error decoding Weather")
                    return
            }
            
            print(weatherInfo)
            
            DispatchQueue.main.async{
                self.label2.text = "\(weatherInfo.date)"
                self.label1.text = "\(weatherInfo.viewTitle)"
                
            }
        }
        task.resume()
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

